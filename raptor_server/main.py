import pymongo
import uvicorn

from fastapi import FastAPI

from models.zillaiot_comp_event import ZillaIot_Event

app=FastAPI(title="zillaiot_raptorAPI", version="0.1",debug=True,)

@app.get("/v1/event/full",response_model=ZillaIot_Event)
async def get_event_full():



    return ZillaIot_Event()

@app.post("/v1/event/full",response_model=ZillaIot_Event)
async def post_event_full(body: ZillaIot_Event):

    body.kind = "blackbox_zillaiot"




    return body

if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000,debug=True,log_level="debug")
