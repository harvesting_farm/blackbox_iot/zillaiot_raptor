# generated by datamodel-codegen:
#   filename:  zillaiot_component_event.schema.json
#   timestamp: 2021-04-08T01:41:53+00:00

from __future__ import annotations

from typing import List, Union

from pydantic import BaseModel


class Overseer(BaseModel):
    name: str
    id: str
    sholve: List[int]
    gas: List[int]
    oil: List[int]
    motor: List[int]
    plate_truck: str
    invoice: str
    status: str
    timestamp: str


class Tracker(BaseModel):
    timestamp: str
    default: int
    status: int
    msg: str
    overseer: Overseer


class North(BaseModel):
    latitude: int
    longitude: int


class Est(BaseModel):
    latitude: int
    longitude: int


class West(BaseModel):
    latitude: int
    longitude: int


class South(BaseModel):
    latitude: int
    longitude: int


class Gps(BaseModel):
    timestamp: str
    north: North
    est: Est
    west: West
    south: South
    speed: int
    status: str


class Video(BaseModel):
    timestamp: str
    file_name: str
    status: str


class Photo(BaseModel):
    timestamp: str
    file_name: str
    status: str


class Events(BaseModel):
    tracker: Tracker
    gps: Gps
    video: Video
    photo: Photo


class ZillaIot_Event(BaseModel):
    kind: str
    mac: str
    key: str
    timestamp: str
    events: Events
    status: List[Union[str, str]]
