# generated by datamodel-codegen:
#   filename:  zillaiot_component.schema.json
#   timestamp: 2021-04-08T01:28:07+00:00

from __future__ import annotations

from typing import Any, Dict, List, Union

from pydantic import BaseModel


class Status(BaseModel):
    default: int
    status: int
    msg: str


class Status1(BaseModel):
    default: int
    status: int
    msg: str


class Status2(BaseModel):
    default: int
    status: int
    msg: str


class ChecklistItems(BaseModel):
    sholve: List[Union[int, int]]
    gas: List[Union[int, int]]
    oil: List[Union[int, int]]
    motor: List[Union[int, int]]
    plate_truck: str
    invoice: str
    status: List[Union[str, str, str]]
    timestamp: str


class Overseer(BaseModel):
    name: str
    id: str
    checklist_items: ChecklistItems


class Overseer1(BaseModel):
    chechlist_items: Dict[str, Any]


class Events(BaseModel):
    timestamp: str
    status: Dict[str, Any]
    overseer: Overseer1


class Tracker(BaseModel):
    name: str
    status: List[Union[Status, Status1, Status2]]
    overseer: Overseer
    events: Events


class Noth(BaseModel):
    latitude: int
    longitude: int


class West(BaseModel):
    latitude: int
    longitude: int


class Soulth(BaseModel):
    latitude: int
    longitude: int


class Est(BaseModel):
    latitude: int
    longitude: int


class Perimeter(BaseModel):
    noth: Noth
    west: West
    soulth: Soulth
    est: Est


class Speed(BaseModel):
    max_allow: int
    min_allow: int


class Events1(BaseModel):
    timestamp: str
    name: str
    perimeter: Perimeter
    speed: Speed


class Gps(BaseModel):
    name: str
    status: List[Union[str, str]]
    events: Events1


class Event(BaseModel):
    timestamp: str
    name: str
    file_name: str
    status: str


class Video(BaseModel):
    name: str
    base_path: str
    time_stamp: str
    prefix_metadata: str
    record_max_time_sec: int
    interval_record_in_secs: int
    status: List[Union[str, str, str, str, str]]
    events: List[Event]


class Event1(BaseModel):
    name: str
    timestamp: str
    file_name: str
    status: str


class Photo(BaseModel):
    name: str
    base_path: str
    time_stamp: str
    prefix_metadata: str
    interval_photo_in_secs: int
    status: List[Union[str, str, str, str]]
    events: List[Event1]


class Config(BaseModel):
    components: List[Union[str, str, str, str]]
    tracker: Tracker
    gps: Gps
    video: Video
    photo: Photo


class Overseer2(BaseModel):
    name: str
    id: str
    sholve: List[int]
    gas: List[int]
    oil: List[int]
    motor: List[int]
    plate_truck: str
    invoice: str
    status: str
    timestamp: str


class Tracker1(BaseModel):
    timestamp: str
    default: int
    status: int
    msg: str
    overseer: Overseer2


class North(BaseModel):
    latitude: int
    longitude: int


class Est1(BaseModel):
    latitude: int
    longitude: int


class West1(BaseModel):
    latitude: int
    longitude: int


class South(BaseModel):
    latitude: int
    longitude: int


class Gps1(BaseModel):
    timestamp: str
    north: North
    est: Est1
    west: West1
    south: South
    speed: int
    status: str


class Video1(BaseModel):
    timestamp: str
    file_name: str
    status: str


class Photo1(BaseModel):
    timestamp: str
    file_name: str
    status: str


class Events2(BaseModel):
    tracker: Tracker1
    gps: Gps1
    video: Video1
    photo: Photo1


class Model(BaseModel):
    kind: str
    mac: str
    key: str
    timestamp: str
    config: Config
    events: Events2
    status: List[Union[str, str]]
