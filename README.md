# Json data structure to define a iot device, your identified data, your components built-int and your captures events data

```json
{
  "kind": "blackbox-iot",
  "mac": "fe80::651b:9fe9:d107:8f86",
  "key": "ZmU4MDo6NjUxYjo5ZmU5OmQxMDc6OGY4Ni8K",
  "timestamp": "",
  "config": {
    "components": [
      "tracker",
      "gps",
      "video",
      "photo"
    ],
    "tracker": {
      "name": "harvest",
      "status": [
        {
          "default": 0,
          "status": 0,
          "msg": "harvest stand by"
        },
        {
          "default": 0,
          "status": 1,
          "msg": "harvest on"
        },
        {
          "default": 0,
          "status": 2,
          "msg": "harvest on NOK"
        }
      ],
      "overseer": {
        "name": "",
        "id": "",
        "checklist_items": {
          "sholve": [
            0,
            1
          ],
          "gas": [
            0,
            1
          ],
          "oil": [
            0,
            1
          ],
          "motor": [
            0,
            1
          ],
          "plate_truck": "#########",
          "invoice": "",
          "status": [
            "OK",
            "NOK",
            "GAP"
          ],
          "timestamp": ""
        }
      },
      "events": {
        "timestamp": "",
        "status": {},
        "overseer": {
          "chechlist_items": {}
        }
      }
    },
    "gps": {
      "name": "harvest",
      "status": [
        "stop",
        "on_the_move"
      ],
      "events": {
        "timestamp": "",
        "name": "harvest",
        "perimeter": {
          "noth": {
            "latitude": 0,
            "longitude": 0
          },
          "west": {
            "latitude": 0,
            "longitude": 0
          },
          "soulth": {
            "latitude": 0,
            "longitude": 0
          },
          "est": {
            "latitude": 0,
            "longitude": 0
          }
        },
        "speed": {
          "max_allow": 0,
          "min_allow": 0
        }
      }
    },
    "video": {
      "name": "harvest",
      "base_path": "",
      "time_stamp": "",
      "prefix_metadata": "harvest-",
      "record_max_time_sec": 600,
      "interval_record_in_secs": 120,
      "status": [
        "recording",
        "in-transfer",
        "pending",
        "error",
        "sync"
      ],
      "events": [
        {
          "timestamp": "",
          "name": "harvest",
          "file_name": "",
          "status": "recording"
        }
      ]
    },
    "photo": {
      "name": "harvest",
      "base_path": "",
      "time_stamp": "",
      "prefix_metadata": "harvest-",
      "interval_photo_in_secs": 10,
      "status": [
        "in-transfer",
        "pending",
        "error",
        "sync"
      ],
      "events": [
        {
          "name": "harvest",
          "timestamp": "",
          "file_name": "",
          "status": "sync"
        }
      ]
    }
  },
  "events": {
    "tracker": {
      "timestamp": "20210331T210421",
      "default": 0,
      "status": 1,
      "msg": "harvest on",
      "overseer": {
        "name": "Rogerio",
        "id": "ererererere",
        "sholve": [
          0
        ],
        "gas": [
          0
        ],
        "oil": [
          0
        ],
        "motor": [
          1
        ],
        "plate_truck": "#########",
        "invoice": "$$$$$$$$$$$$$$$",
        "status": "NOK",
        "timestamp": ""
      }
    },
    "gps": {
      "timestamp": "20210331T210421",
      "north": {
        "latitude": 0,
        "longitude": 0
      },
      "est": {
        "latitude": 0,
        "longitude": 0
      },
      "west": {
        "latitude": 0,
        "longitude": 0
      },
      "south": {
        "latitude": 0,
        "longitude": 0
      },
      "speed": 10,
      "status": "on_the_move"
    },
    "video": {
      "timestamp": "20210331T210421",
      "file_name": "harvest-20210401120000.mov",
      "status": "recording"
    },
    "photo": {
      "timestamp": "20210331T210421",
      "file_name": "harvest-20210401120000.png",
      "status": "sync"
    }
  },
  "status": [
    "pending",
    "sync"
  ]
}
```
